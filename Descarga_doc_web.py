import urllib.request as url_req


# Clase Robot
class Robot:

    def __init__(self, _url):
        self.url = _url
        self.download = False

    def retrieve(self):
        if not self.download:
            print(f"Descargando {self.url}")
            self.archivo_tmp = url_req.urlretrieve(self.url)
            self.download = True

    def show(self):
        self.retrieve()
        f = open(self.archivo_tmp[0], "r")
        print(f.read())
        f.close()

    # Esta mal
    def content(self):
        self.retrieve()
        return self.archivo_tmp[0]


# Clase Cache
class Cache:

    def __init__(self):
        self.cache = {}

    def retrieve(self, _url):
        if _url not in self.cache:
            robot = Robot(_url=_url)
            self.cache[_url] = robot

    def show(self, _url):
        self.retrieve(_url)
        print(self.cache[_url].show())

    def show_all(self):
        print(self.cache.keys())
        for url in self.cache:
            print(url)

    # Esta mal
    def content(self, _url):
        self.retrieve(_url)
        return self.cache[_url].content()
# main
if __name__ == '__main__':
    print("Robot Class Test: ")
    r1 = Robot('http://gsyc.urjc.es/')
    print(f"URL introducida: {r1.url}")
    print("")
    r1.show()
    r1.retrieve()
    r1.retrieve()
    r1.content()

    r2 = Robot("https://www.aulavirtual.urjc.es/moodle/pluginfile.php/11128909/mod_resource/content/7/RM2223-Tema0.pdf")
    r2.retrieve()
    r2.show()

    c1 = Cache()
    c1.retrieve('http://gsyc.urjc.es/')
    print(c1.cache)
    c1.show('https://www.aulavirtual.urjc.es')
    c1.show_all()

